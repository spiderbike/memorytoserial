﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MemoryToSerial
{
    class Memory
    {

        public Process TheProcess { get; set; }
        public Memory(Process runningProcess)
        {
            this.TheProcess = runningProcess;
        }



        //function ReadString (returns string value)
        public string ReadString(ProcessConfig.MemoryItem memoryItem)
        {

            var procHandle = TheProcess.Handle;

            IntPtr baseAddress = TheProcess.MainModule.BaseAddress;


            IntPtr ptr;
            if (memoryItem.Address.Contains("+"))
            {
                //We have an address relative to base
                ptr = baseAddress + int.Parse(memoryItem.Address.Split('+')[1].Trim(), NumberStyles.HexNumber);
            }
            else
            {
                //We have a static address?
                ptr = baseAddress + int.Parse(memoryItem.Address, NumberStyles.HexNumber);
            }



            foreach (var strOffset in Enumerable.Reverse(memoryItem.Offsets))
            {
                var newPtr = ReadPointer(procHandle, ptr);
                Console.WriteLine($"*({ptr.ToString("X8")}) = {newPtr.ToString("X8")}");
                ptr = newPtr;

                int offset = int.Parse(strOffset, NumberStyles.HexNumber);

                newPtr = ptr + offset;
                Console.WriteLine($"{ptr.ToString("X8")} + {offset:X} = {newPtr.ToString("X8")}");
                ptr = newPtr;
            }
            
            float value = ReadFloat(procHandle, ptr);
            return value.ToString();
        }

        private static IntPtr ReadPointer(IntPtr procHandle, IntPtr address)
        {
            byte[] buffer = new byte[IntPtr.Size];
            int read = -1;

            if (!ReadProcessMemory(procHandle, address, buffer, buffer.Length, out read))
                throw new Win32Exception();

            if (read == buffer.Length)
            {
                if (IntPtr.Size == sizeof(Int32))
                {
                    return new IntPtr(BitConverter.ToInt32(buffer, 0));
                }
                else if (IntPtr.Size == sizeof(Int64))
                {
                    return new IntPtr(BitConverter.ToInt64(buffer, 0));
                }
                else
                {
                    throw new Exception($"Can't handle weird architecture: IntPtr.Size is {IntPtr.Size}");
                }
            }
            else
            {
                throw new Exception($"ReadProcessMemory only read {read} bytes out of {buffer.Length}");
            }
        }

        private static float ReadFloat(IntPtr procHandle, IntPtr address)
        {
            byte[] buffer = new byte[sizeof(float)];
            int read = -1;

            if (!ReadProcessMemory(procHandle, address, buffer, buffer.Length, out read))
                throw new Win32Exception();

            if (read == buffer.Length)
            {
                return BitConverter.ToSingle(buffer, 0);
            }
            else
            {
                throw new Exception($"ReadProcessMemory only read {read} bytes out of {buffer.Length}");
            }
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ReadProcessMemory(
   IntPtr hProcess,
   IntPtr lpBaseAddress,
   [Out] byte[] lpBuffer,
   int dwSize,
   out int lpNumberOfBytesRead);

    }


}

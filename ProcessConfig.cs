﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MemoryToSerial
{
  public class ProcessConfig
    {

        public static readonly XmlSerializer Serializer = new XmlSerializer(typeof(ProcessConfig));


        public string Title { get; set; }
        public string ProcessName { get; set; }
        public List<MemoryItem> MemoryItems { get; set; }

        public class MemoryItem
        {
            public string Title { get; set; }
            public MemType Type { get; set; }
            public string Address { get; set; }
            public string OutputTitle { get; set; }
            public MemType OutputType { get; set; }
            public string OutputValue { get; set; }
            [XmlArrayItem("Offset")]
            public List<string> Offsets { get; set; }

            public enum MemType { Double,String, Int};
        }

    



        public static ProcessConfig Deserialize(string xml)
        {

            using (TextReader reader = new StringReader(xml))
            {
                var processConfig = (ProcessConfig)Serializer.Deserialize(reader);
                return processConfig;
            }
        }

        public void WriteConfig()
        {
            var conf = new Config();
            File.WriteAllText(Path.Combine(conf.ConfigPath,Title+ ".xml"),Serialize(this));
        }


        public static string Serialize(ProcessConfig processConfig)
        {

            using (var textWriter = new StringWriter())
            {
                Serializer.Serialize(textWriter, processConfig);
                return textWriter.ToString();
            }
        }
    }
}

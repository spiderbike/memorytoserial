﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryToSerial
{
    internal class Config
    {
        public Config()
        {
            ConfigPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "configs");
        }

        public string ConfigPath { get; private set; }


        public List<ProcessConfig> LoadConfigs()
        {
            var configFiles = Directory.GetFiles(ConfigPath, "*.xml", SearchOption.AllDirectories);
            return configFiles.Select(file => ProcessConfig.Deserialize(File.ReadAllText(file))).ToList();

        }
    }


}

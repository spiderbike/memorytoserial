﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MemoryToSerial
{
    class Serial
    {
        // Create the serial port with basic settings
        private SerialPort port = new SerialPort("COM1", 9600, Parity.None, 8, StopBits.One);
        private string response = "";
        private Boolean PortValid = false;


        public Serial()
        {

        }

        public SerialPort OpenCorrectDevice()
        {
            while (true)
            {
                foreach (string comPort in SerialPort.GetPortNames())
                {
                    response = "";
                    Console.WriteLine($"Checking Device on {comPort}");

                    // Attach a method to be called when there
                    // is data waiting in the port's buffer
                    port.DataReceived += new
                      SerialDataReceivedEventHandler(port_DataReceived);
                    port.PortName = comPort;
                    // Begin communications
                    port.Open();

                    // Write a string
                    port.Write("STATUS\n");
                    Thread.Sleep(1000);
                    if (response.Contains("Ok."))
                    {
                        Console.WriteLine($"{comPort} is Valid.");
                        return port;
                    }
                    port.Close();
                    if (PortValid)
                    {
                        return port;
                    }
                }
            }
        }


        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Show all the incoming data in the port's buffer
            response += port.ReadExisting();
            PortValid = true;
        }

        internal void Write(ProcessConfig.MemoryItem memoryItem, string memoryValue)
        {
            //if (Int32.TryParse(memoryValue, out int newMemoryValue))
            //{

            port.Write(memoryItem.OutputTitle + " " + (int)Convert.ToDouble(memoryValue) +"\n");
            Console.WriteLine("Sending the following to "+port.PortName+" "+ memoryItem.OutputTitle + " " + (int)Convert.ToDouble(memoryValue)+"\n");
            //}
            //else
            //{
            //    port.Write($"{memoryItem.OutputTitle} 0");
            //}

        }
    }
}
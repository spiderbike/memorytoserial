﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static MemoryToSerial.ProcessConfig;
using static MemoryToSerial.ProcessConfig.MemoryItem;

namespace MemoryToSerial
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {

            //var pc = new ProcessConfig
            //{
            //    Title = "Blur Racing Game",
            //    ProcessName = "Blur"
            //};


            //var offsets = new List<string>();
            //offsets.Add("978");

            //var memoryItem = new ProcessConfig.MemoryItem
            //{
            //    Title = "Speed",
            //    Type = MemType.Double,
            //    OutputTitle = "Speed",
            //    OutputType = MemType.Double,
            //    Address = @"""Blur.exe""+ 00D32D0C",
            //    Offsets = offsets
            //};


            //var offsets2 = new List<string>();
            //offsets2.Add("534");
            //offsets2.Add("B90");
            //offsets2.Add("C");
            //offsets2.Add("18");
            //offsets2.Add("24");
            //var memoryItem2 = new ProcessConfig.MemoryItem
            //{
            //    Title = "complicated 5 step pointer",
            //    Type = MemType.Double,
            //    OutputTitle = "Speed2",
            //    OutputType = MemType.Double,
            //    Address = @"""Blur.exe"" +00C28D28 ",
            //    Offsets = offsets2
            //};

            //List<ProcessConfig.MemoryItem> memoryItems = new List<ProcessConfig.MemoryItem> { memoryItem,memoryItem2 };
            //pc.MemoryItems = memoryItems;

            //pc.WriteConfig();


            //var comPorts = SerialPort.GetPortNames();


            //foreach (string comPort in SerialPort.GetPortNames())
            //{
            //    SerialPort port = new SerialPort(comPort, 9600, Parity.None, 8, StopBits.One);
            //    // Open the port for communications
            //    port.Open();

            //    // Write a string
            //    port.Write("Hello World");
            //    port.Close();
            //    Console.WriteLine(comPort);
            //}




            var config = new Config();
            CheckSupportedProcess(config);
        }





        private static void CheckSupportedProcess(Config config)
        {
            var serial = new Serial();
            serial.OpenCorrectDevice();
            var configs = config.LoadConfigs();
            OutPut.Write(0, 1, "Searching for supported Processes");
            OutPut.Write(0, 2, $"{configs.Count} Configs Loaded");


            while (true)
            {


                // Get all processes running on the local computer.
                var runningProcesses = Process.GetProcesses();
                var row = 4;
                bool foundRunningProcess = false;
                foreach (var runningProcess in runningProcesses)
                {
                    if (foundRunningProcess)
                    {
                        break;
                    }
                    foreach (var configuration in configs)
                    {
                        if (runningProcess.ProcessName == configuration.ProcessName)
                        {
                            foundRunningProcess = true;
                            OutPut.Write(0, row + 1, $"Running process {configuration.ProcessName} found for {configuration.Title}, Running:{runningProcess.ProcessName == configuration.ProcessName}");

                            Thread.Sleep(4000);
                            try
                            {

                                var memory = new Memory(runningProcess);

                                while (true)
                                {
                                  //  Console.Clear();
                                    Process[] pname = Process.GetProcessesByName(configuration.ProcessName);
                                    if (pname.Length == 0)
                                    {
                                   //     Console.Clear();
                                        OutPut.Write(0, 1, "Searching for supported Processes");
                                        OutPut.Write(0, 2, $"{configs.Count} Configs Loaded");
                                        Thread.Sleep(4000);
                                        break;
                                    }
                                    row = 8;
                                    foreach (var memoryItem in configuration.MemoryItems)
                                    {
                                        row++;
                                        Console.Write($"{memoryItem.Title}: ");
                                        try
                                        {
                                            var memoryValue = memory.ReadString(memoryItem);
                                            Console.WriteLine(memoryItem.OutputTitle + memoryValue);
                                            serial.Write(memoryItem, memoryValue);

                                        }
                                        catch (Exception ex)
                                        {
                                            Console.WriteLine("+++ EXCEPTION +++\n" + ex.ToString());
                                        }

                                        //Console.WriteLine($"{memoryItem.Title}: {memory.ReadString(memoryItem)}"); ;
                                        //OutPut.Write(0, row, $"{memoryItem.Title} :{memory.ReadString(memoryItem)}");

                                    }
                                    Thread.Sleep(100);
                                }

                            }
                            catch (Exception exception)
                            {
                                Console.Clear();
                                OutPut.Write(0, 0, exception.ToString());
                                Thread.Sleep(4000);
                            }

                            break;
                        }
                    }
                }


                Thread.Sleep(1000);
            }
        }
    }
}
